package com.pulmuone.knowledge.worker.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pulmuone.knowledge.worker.R;
import com.pulmuone.knowledge.worker.auth.LoginApiInterface;
import com.pulmuone.knowledge.worker.auth.LogoutApiInterface;
import com.pulmuone.knowledge.worker.auth.bean.LoginRequestDTO;
import com.pulmuone.knowledge.worker.auth.bean.LoginResponseDTO;
import com.pulmuone.knowledge.worker.auth.bean.LogoutRequestDTO;
import com.pulmuone.knowledge.worker.auth.bean.LogoutResponseDTO;
import com.pulmuone.knowledge.worker.common.AMSettings;
import com.pulmuone.knowledge.worker.common.BaseFragment;
import com.pulmuone.knowledge.worker.common.DefaultRestClient;
import com.pulmuone.knowledge.worker.common.DialogHelper;
import com.pulmuone.knowledge.worker.common.GetCommCodeListApiInterface;
import com.pulmuone.knowledge.worker.common.PreferenceManager;
import com.pulmuone.knowledge.worker.common.Utils;
import com.pulmuone.knowledge.worker.common.bean.CommCodeDTO;
import com.pulmuone.knowledge.worker.common.bean.GetCommCodeListRequestDTO;
import com.pulmuone.knowledge.worker.common.bean.GetCommCodeListResponseDTO;
import com.pulmuone.knowledge.worker.main.bean.AddWorkInOutRequestDTO;
import com.pulmuone.knowledge.worker.main.bean.AddWorkInOutResponseDTO;
import com.pulmuone.knowledge.worker.schedule.GetTaskListApiInterface;
import com.pulmuone.knowledge.worker.schedule.ScheduleDetailActivity;
import com.pulmuone.knowledge.worker.schedule.ScheduleDetailFragment;
import com.pulmuone.knowledge.worker.schedule.ScheduleListActivity;
import com.github.ajalt.timberkt.Timber;
import com.pulmuone.knowledge.worker.schedule.bean.GetTaskListRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.GetTaskListResponseDTO;
import com.pulmuone.knowledge.worker.schedule.bean.TaskListItemDTO;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.grantland.widget.AutofitTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Main Fragment
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseFragment
 */
public class MainFragment extends BaseFragment {
    private final String TAG = MainFragment.class.getSimpleName();

    /**
     * 위치 권한 Request Code
     */
    private static final int LOCATION_PERMISSION_REQUEST = 10;

    /**
     * 출/퇴근 API Request Code
     */
    private final int REQUEST_CODE_ADD_WORK_IN_OUT = 20;

    /**
     * 출/퇴근 버튼 클릭 여부
     */
    private final int WORK_MODE_IN = 0;
    private final int WORK_MODE_OUT = 1;
    private int WORK_MODE = -1;

    /**
     * TASK 목록 ErrorType Code
     */
    private final int REQUEST_CODE_GET_TASK_LIST = 103;

    private AutofitTextView userInfoUserName;
    private AutofitTextView userInfoUserPart;
    private TextView userInfoLogOutButton;

    private TextView dateInfoDate;
    private TextView dateInfoTime;

    private ImageButton onWorkButton;
    private ImageButton offWorkButton;
    private TextView workMessage;
    private LinearLayout workAlertLayout;
    private TextView workAlertText;

    private LinearLayout showScheduleButton;
    private LinearLayout addScheduleButton;

    /**
     * Location
     */
    // Current Location 을 가져오고 있는지 여부
    private boolean mRequestingLocationUpdates = false;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationManager locationManager;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * Expired duration for location updates.
     */
    private static final long EXPIRED_LOCATION_MILLISECONDS = 15000;


    private double current_lat;
    private double current_lon;
    private String current_address;

    private List<CommCodeDTO> workTypeList;
    private List<CommCodeDTO> taskList;

    /**
     * Handler for WeakReference
     */
    private static class MainHandler extends Handler {
        private final WeakReference<MainFragment> weakReference;

        public MainHandler(MainFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        public MainFragment getWeakReference() {
            return weakReference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            MainFragment theFrag = weakReference.get();
            if(theFrag != null) {
                theFrag.handleMessage(msg);
            }
        }
    }

    private MainHandler mHandler = new MainHandler(this);

    private boolean canClick = true;

    private void handleMessage(Message msg) {
        // NOTHING
    }

    /**
     * Default Constructor
     *
     * @param userInfo      회원 정보
     * @param workInOut     출퇴근 목록
     * @return              MainFragment Instance
     */
    public static MainFragment getInstance(String userInfo, String workInOut) {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        bundle.putString("USER_INFO", userInfo);
        bundle.putString("WORK_IN_OUT", workInOut);
        fragment.setArguments(bundle);
        return fragment;
    }

    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity) {
            mainActivity = (MainActivity) context;
        }
    }

    /**
     * Get Root Layout
     *
     * @return Layout of this Fragment
     */
    @Override
    public int getLayout() {
        return R.layout.fragment_main;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.tag(TAG).i("onCreateView");

        View contentView = super.onCreateView(inflater, container, savedInstanceState);

        userInfoUserName = (AutofitTextView) contentView.findViewById(R.id.main_contents_user_info_user_name);
        userInfoUserPart = (AutofitTextView) contentView.findViewById(R.id.main_contents_user_info_user_part);
        userInfoLogOutButton = (TextView) contentView.findViewById(R.id.main_contents_user_info_logout_button);

        // AMSettings.useOnOff == false 일 경우 주석 [START]
        dateInfoDate = (TextView) contentView.findViewById(R.id.main_contents_date_info_date);
        dateInfoTime = (TextView) contentView.findViewById(R.id.main_contents_date_info_time);

        onWorkButton = (ImageButton) contentView.findViewById(R.id.main_contents_work_info_onwork_button);
        offWorkButton = (ImageButton) contentView.findViewById(R.id.main_contents_work_info_offwork_button);

        workMessage = (TextView) contentView.findViewById(R.id.main_contents_work_info_message);
        workAlertLayout = (LinearLayout) contentView.findViewById(R.id.main_contents_work_info_alert_layout);
        workAlertText = (TextView) contentView.findViewById(R.id.main_contents_work_info_alert_text);
        // AMSettings.useOnOff == false 일 경우 주석 [END]

        showScheduleButton = (LinearLayout) contentView.findViewById(R.id.main_bottom_show_schedule_button_layout);
        addScheduleButton = (LinearLayout) contentView.findViewById(R.id.main_bottom_add_schedule_button_layout);

        /**
         * ClickListener of LogOut Button
         */
        userInfoLogOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClick) {
                    setCanClickable();

                    if(mainActivity != null) {
                        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity).setMessage(R.string.logout_confirm_message).setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                // NOTHING
                            }
                        }).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                logout();
                            }
                        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Nothing
                            }
                        }).create();
                        alertDialog.show();
                    }
                }
            }
        });

        /**
         * ClickListener of Show Schedule Button
         */
        showScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClick) {
                    setCanClickable();

                    if(mainActivity != null) {
                        Intent intent = new Intent();
                        intent.setClass(mainActivity, ScheduleListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
            }
        });

        /**
         * ClickListener of Add Schedule Button
         */
        addScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClick) {
                    setCanClickable();

                    if (mainActivity != null) {
                        Intent intent = new Intent();
                        intent.setClass(mainActivity, ScheduleDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("MODE", ScheduleDetailFragment.MODE_ADD);
                        startActivity(intent);
                    }
                }
            }
        });

        if(AMSettings.useOnOffWork) {
            /**
             * ClickListener of onWork Button
             */
            onWorkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(canClick) {
                        setCanClickable();

                        WORK_MODE = WORK_MODE_IN;
                        doCurrentLocation();
                    }
                }
            });

            /**
             * ClickListener of offWork Button
             */
            offWorkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(canClick) {
                        setCanClickable();

                        WORK_MODE = WORK_MODE_OUT;
                        doCurrentLocation();
                    }
                }
            });
        }

        return contentView;
    }

    /**
     * showScheduleButton 클릭 이벤트 호출
     */
    public void callOnButtonClick(Serializable currentDate) {
//        showScheduleButton.callOnClick();
        if(canClick) {
            setCanClickable();

            if(mainActivity != null) {
                Intent intent = new Intent();
                intent.setClass(mainActivity, ScheduleListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("CURRENT_DATE", currentDate);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mainActivity != null) {
            mainActivity.startPostponedEnterTransition();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Timber.tag(TAG).i("onActivityCreated");

        setLayoutFromData();

        /**
         * Location 관련 Initialize
         */
        if(mainActivity != null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mainActivity);
            locationManager = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
            createLocationCallback();
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getWorkTypeList();
                getTaskList();
            }
        }, 0);
    }

    @Override
    public void onResume() {
        super.onResume();

        Timber.tag(TAG).i("onResume");

        /**
         * Resume 시 Location 정보를 가져오던 중이었는지 체크를 한다.
         */
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        } else if (mRequestingLocationUpdates && !checkPermissions()) {
            requestPermissions(LOCATION_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        Timber.tag(TAG).i("onPause");

        /**
         * Pause 시 Location 정보를 가져오던 것을 Stop 한다.
         */
        stopLocationUpdates();
    }

    @Override
    public void onDestroy() {
        Timber.tag(TAG).i("onDestroy");

        // 혹시 onPause 에서 stop 이 안되었을 경우를 대비하여 방어코드...
        stopLocationUpdates();

        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case LOCATION_PERMISSION_REQUEST: {
                if ((grantResults.length > 1 && ((grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        && (grantResults[1] == PackageManager.PERMISSION_GRANTED))) ||
                        (grantResults.length > 0 && ((grantResults[0] == PackageManager.PERMISSION_GRANTED)))) {
                    Timber.tag(TAG).d("[onRequestPermissionsResult] Permission Granted");
                    doCurrentLocation();
                } else {
                    // 퍼미션 거부 했을 때
                    Timber.tag(TAG).e("[onRequestPermissionsResult] Permission Not Granted");
                    initCurrentLocation();
                    mRequestingLocationUpdates = false;
                    addWorkInOut();
                }
                break;
            }
        }
    }

    /**
     * 연속 클릭 방지
     */
    private void setCanClickable() {
        canClick = false;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                canClick = true;
            }
        }, 500);
    }

    /**
     * 가져온 Data 를 가지고 Layout 에 보여준다.
     */
    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
    private void setLayoutFromData() {
        if(getArguments() != null) {
            String userInfoString = getArguments().getString("USER_INFO");
            String workInOutString = "";

            if(AMSettings.useOnOffWork) {
                workInOutString = getArguments().getString("WORK_IN_OUT");
            }

            if (userInfoString != null && userInfoString.length() > 0) {
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginResponseDTO.UserInfo>(){}.getType();
                    LoginResponseDTO.UserInfo userInfo = gson.fromJson(userInfoString, type);

                    String userName = Utils.nullCheck(userInfo.getEmpName()) + " " + Utils.nullCheck(userInfo.getPosition());
                    userInfoUserName.setText(userName);

                    userInfoUserPart.setText(Utils.nullCheck(userInfo.getPrmryDivNm()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd(E)", Locale.KOREAN);
            SimpleDateFormat tdf = new SimpleDateFormat("HH:mm");

            if (workInOutString != null && workInOutString.length() > 0) {
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<LoginResponseDTO.WorkInOutList>>(){}.getType();
                    List<LoginResponseDTO.WorkInOutList> workInOutList = gson.fromJson(workInOutString, type);

                    if (workInOutList.size() > 0) {
                        String startTimeString = "";
                        String endTimeString = "";
                        boolean isOffWork = false;    // 퇴근 여부

                        for (LoginResponseDTO.WorkInOutList item : workInOutList) {
                            try {
                                String date = item.getWrkInOutDate();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                                Date mDate = dateFormat.parse(date);

                                String mDateString = sdf.format(mDate);
                                dateInfoDate.setText(mDateString);

                                String time = item.getWrkInOutTime();

                                SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
                                Date mTime = timeFormat.parse(time);

                                if (item.getWrkInOutStatus().equalsIgnoreCase("IN")) {
                                    // 출근
                                    startTimeString = tdf.format(mTime);
                                } else {
                                    // 퇴근
                                    endTimeString = tdf.format(mTime);
                                    isOffWork = true;
                                }
                            } catch (Exception de) {
                                de.printStackTrace();
                            }
                        }

                        dateInfoTime.setText(startTimeString + " ~ " + endTimeString);
                        if (isOffWork) {
                            workMessage.setText(R.string.main_off_work_message);
                        } else {
                            workMessage.setText(R.string.main_on_work_message);
                        }
                    } else {
                        // 출/퇴근 기록이 없으면 오늘날짜만 표시 해준다.
                        Date today = new Date();
                        dateInfoDate.setText(sdf.format(today));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if(AMSettings.useOnOffWork) {
                    Date today = new Date();
                    dateInfoDate.setText(sdf.format(today));
                }
            }
        }
    }

    /**
     * 회원 정보가 업데이트 되면 Layout 쪽도 업데이트 해준다.
     *
     * @param userInfoString 회원 정보
     * @param workInOutListString 출퇴근 목록
     */
    public void refreshUserInfo(String userInfoString, String workInOutListString) {
        if(getArguments() != null) {
            getArguments().putString("USER_INFO", userInfoString);
            getArguments().putString("WORK_IN_OUT", workInOutListString);

            setLayoutFromData();
        }
    }

    /**
     * Login
     *
     * @param requestCode   RequestCode of Login
     * @param stringParam   Param of Login
     */
    @Override
    public void doAuthentication(final int requestCode, String stringParam) {
        LoginRequestDTO requestDTO = new LoginRequestDTO();
        requestDTO.setUserId(PreferenceManager.getUserId(mainActivity));
        requestDTO.setUserPW(Utils.EncBySha256(PreferenceManager.getUserPwd(mainActivity)));

        Timber.tag(TAG).d("[doAuthentication] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<LoginApiInterface> loginRestClient = new DefaultRestClient<>();
        LoginApiInterface loginInterface = loginRestClient.getClient(LoginApiInterface.class);

        Call<LoginResponseDTO> call = loginInterface.doLogin(requestDTO);
        call.enqueue(new Callback<LoginResponseDTO>() {
            @Override
            public void onResponse(Call<LoginResponseDTO> call, Response<LoginResponseDTO> response) {
                hideLoadingProgressBar();

                if(response.isSuccessful()) {
                    LoginResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[doAuthentication] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            String userInfoString = Utils.convertObjToJSON(responseDTO.getUserInfo());
                            Timber.tag(TAG).d("[doAuthentication][USER_INFO] %s", userInfoString);

                            String workInOutListString = Utils.convertObjToJSON(responseDTO.getWorkInOutList());
                            Timber.tag(TAG).d("[doAuthentication][WORK_IN_OUT] %s", workInOutListString);

                            refreshUserInfo(userInfoString, workInOutListString);

                            switch(requestCode) {
                                case REQUEST_CODE_ADD_WORK_IN_OUT: {
                                    retryAddWorkInOut();

                                    break;
                                }
                            }
                        } else {
                            switch (responseDTO.getErrorType()) {
                                default: {
                                    Timber.tag(TAG).e("[doAuthentication] ERROR TYPE : %s", responseDTO.getErrorType());

                                    DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(responseDTO.getMessage()));

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[doAuthentication] RESPONSE BODY IS NULL");

                        DialogHelper.showNormalAlertDialog(mainActivity, "");

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[doAuthentication] RESPONSE FAIL code : %s", response.code());

                    DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(response.message()));

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<LoginResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[doAuthentication][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                DialogHelper.showNormalAlertDialog(mainActivity, "");

                setReLogInCount(0);
            }
        });
    }

    /**
     * 로그아웃
     */
    private void logout() {
        showLoadingProgressBar();

        LogoutRequestDTO requestDTO = new LogoutRequestDTO();

        Timber.tag(TAG).d("[logout] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<LogoutApiInterface> logoutRestClient = new DefaultRestClient<>();
        LogoutApiInterface logoutInterface = logoutRestClient.getClient(LogoutApiInterface.class);

        Call<LogoutResponseDTO> call = logoutInterface.doLogout(requestDTO);
        call.enqueue(new Callback<LogoutResponseDTO>() {
            @Override
            public void onResponse(Call<LogoutResponseDTO> call, Response<LogoutResponseDTO> response) {
                hideLoadingProgressBar();

                if(response.isSuccessful()) {
                    LogoutResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[logout] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));
                    }

                    try {
                        PreferenceManager.setUserId(mainActivity, "");
                        PreferenceManager.setUserPwd(mainActivity, "");

                        AMSettings.COOKIESET.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    goLogin();
                } else {
                    Timber.tag(TAG).e("[logout] RESPONSE FAIL code : %s", response.code());

                    try {
                        PreferenceManager.setUserId(mainActivity, "");
                        PreferenceManager.setUserPwd(mainActivity, "");

                        AMSettings.COOKIESET.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    goLogin();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[logout][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                try {
                    PreferenceManager.setUserId(mainActivity, "");
                    PreferenceManager.setUserPwd(mainActivity, "");

                    AMSettings.COOKIESET.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                goLogin();
            }
        });
    }

    /**
     * 근무일정 저장 재시도
     */
    private void retryAddWorkInOut() {
        setReLogInCount(getReLogInCount() + 1);
        addWorkInOut();
    }

    /**
     * 근무일정 저장
     */
    private void addWorkInOut() {
        showLoadingProgressBar();

        AddWorkInOutRequestDTO requestDTO = new AddWorkInOutRequestDTO();
        requestDTO.setWrkInOutStatus(WORK_MODE == WORK_MODE_IN ? "IN" : "OUT");
        if(current_lat != 0 && current_lon != 0) {
            requestDTO.setLatitudeNum(current_lat);
            requestDTO.setLongitudeNum(current_lon);
            requestDTO.setAddressInfo(current_address);
        }

        Timber.tag(TAG).d("[addWorkInOut] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<AddWorkInOutApiInterface> workInOutRestClient = new DefaultRestClient<>();
        AddWorkInOutApiInterface workInOutInterface = workInOutRestClient.getClient(AddWorkInOutApiInterface.class);

        Call<AddWorkInOutResponseDTO> call = workInOutInterface.addWorkInOut(requestDTO);
        call.enqueue(new Callback<AddWorkInOutResponseDTO>() {
            @Override
            public void onResponse(Call<AddWorkInOutResponseDTO> call, Response<AddWorkInOutResponseDTO> response) {
                if(response.isSuccessful()) {
                    // 성공
                    AddWorkInOutResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[addWorkInOut] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            hideLoadingProgressBar();

                            switch(WORK_MODE) {
                                case WORK_MODE_IN: {
                                    // 위치를 못가져 왔을 때 위치 정보가 꺼져 있어 출근시간만 확인 된다는 메시지를 보여준다.
                                    if(current_lat == 0 && current_lon == 0) {
                                        workAlertText.setText(R.string.main_on_work_alert_message);
                                        showLocationAlertMessage();
                                    }

                                    workMessage.setText(R.string.main_on_work_message);

                                    DialogHelper.showNormalAlertDialog(mainActivity, getResources().getString(R.string.main_on_work_success_message));

                                    break;
                                }
                                case WORK_MODE_OUT: {
                                    // 위치를 못가져 왔을 때 위치 정보가 꺼져 있어 퇴근시간만 확인 된다는 메시지를 보여준다.
                                    if(current_lat == 0 && current_lon == 0) {
                                        workAlertText.setText(R.string.main_off_work_alert_message);
                                        showLocationAlertMessage();
                                    }

                                    workMessage.setText(R.string.main_off_work_message);

                                    DialogHelper.showNormalAlertDialog(mainActivity, getResources().getString(R.string.main_off_work_success_message));

                                    break;
                                }
                            }

                            // Main 의 Layout 도 갱신해준다.
                            if(responseDTO.getWorkInOutList() != null) {
                                if(getArguments() != null) {
                                    getArguments().putString("WORK_IN_OUT", Utils.convertObjToJSON(responseDTO.getWorkInOutList()));

                                    setLayoutFromData();
                                }
                            }

                            setReLogInCount(0);
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if (getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_ADD_WORK_IN_OUT, "");
                                    } else {
                                        hideLoadingProgressBar();

                                        setReLogInCount(0);

                                        DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(responseDTO.getMessage()));
                                    }
                                    break;
                                }
                                default: {
                                    hideLoadingProgressBar();

                                    DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(responseDTO.getMessage()));

                                    setReLogInCount(0);
                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[addWorkInOut] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        DialogHelper.showNormalAlertDialog(mainActivity, "");

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[addWorkInOut] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(response.message()));

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<AddWorkInOutResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[addWorkInOut][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                DialogHelper.showNormalAlertDialog(mainActivity, "");

                setReLogInCount(0);
            }
        });
    }

    /**
     * 위치 정보가 꺼져 있어 출/퇴근 시간만 확인 됩니다 메시지 보여주기
     *
     */
    private void showLocationAlertMessage() {
        final Animation hideAnim = AnimationUtils.loadAnimation(mainActivity, R.anim.main_work_alert_dismiss);
        hideAnim.setInterpolator(mainActivity, android.R.anim.accelerate_interpolator);
        hideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // NOTHING
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                workAlertLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // NOTHING
            }
        });

        Animation showAnim = AnimationUtils.loadAnimation(mainActivity, R.anim.main_work_alert_show);
        showAnim.setInterpolator(mainActivity, android.R.anim.accelerate_interpolator);
        showAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // NOTHING
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Show Animation 이 종료되면 2초 후 Hide Animation 을 시작해준다.
                if(mHandler != null) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(workAlertLayout != null && hideAnim != null) {
                                workAlertLayout.startAnimation(hideAnim);
                            }
                        }
                    }, 2000);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // NOTHING
            }
        });

        if(workAlertLayout != null) {
            workAlertLayout.setVisibility(View.VISIBLE);
            workAlertLayout.startAnimation(showAnim);
        }
    }

    /**
     * LocationRequest 생성
     */
    private LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setMaxWaitTime(EXPIRED_LOCATION_MILLISECONDS);
        return mLocationRequest;
    }

    /**
     * Location Callback 생성
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if(mainActivity != null) {
                    if (locationResult != null && locationResult.getLastLocation() != null) {
                        current_lat = locationResult.getLastLocation().getLatitude();
                        current_lon = locationResult.getLastLocation().getLongitude();
                        current_address = "";

                        // 주소변환
                        Geocoder geocoder = new Geocoder(mainActivity, Locale.getDefault());
                        try {
                            List<Address> addresses = geocoder.getFromLocation(current_lat, current_lon, 1);
                            if(addresses == null || addresses.size() == 0) {
                                Timber.tag(TAG).e("[LocationCallback] Geocoder address is null");
                            } else {
                                current_address = addresses.get(0).getAddressLine(0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        initCurrentLocation();
                    }

                    Timber.tag(TAG).d("[LocationCallback] current_lat : " + current_lat + " current_lon : " + current_lon + " current_address : " + current_address);

                    addWorkInOut();

                    // 한번만 위치 가져온다.
                    stopLocationUpdates();
                }
            }
        };
    }

    /**
     * Initialize Current Location Information
     */
    private void initCurrentLocation() {
        current_lat = 0;
        current_lon = 0;
        current_address = "";
    }

    /**
     * 현재 위치 가져오기
     */
    private void doCurrentLocation() {
        if(!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }
    }

    /**
     * Location Update 시작
     */
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(mainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(mainActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if(!checkPlayServices()) {
                // PlayService 를 사용할 수 없을 때
                Timber.tag(TAG).e("[startLocationUpdates] PlayService is not usable");
                initCurrentLocation();
                mRequestingLocationUpdates = false;
                addWorkInOut();

                return;
            }

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // 위치 꺼져있을 때
                Timber.tag(TAG).e("[startLocationUpdates] GPS Provider is not usable");
                initCurrentLocation();
                mRequestingLocationUpdates = false;
                addWorkInOut();
            } else {
                Timber.tag(TAG).d("[startLocationUpdates] Request Location Updates!!!");
                showLoadingProgressBar();
                mFusedLocationClient.requestLocationUpdates(createLocationRequest(), mLocationCallback, null);
            }
        } else {
            Timber.tag(TAG).d("[startLocationUpdates] requestPermissions");
            requestPermissions(LOCATION_PERMISSION_REQUEST);
        }
    }

    /**
     * Stop Location Update
     */
    private void stopLocationUpdates() {
        if(mainActivity == null || !mRequestingLocationUpdates) return;

        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
            .addOnCompleteListener(mainActivity, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mRequestingLocationUpdates = false;
                }
            });
    }

    /**
     * Permission 체크
     *
     * @return  Is granted Permissions
     */
    private boolean checkPermissions() {
        if(mainActivity != null) {
            int permissionState = ContextCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionState1 = ContextCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
            return (permissionState == PackageManager.PERMISSION_GRANTED) && (permissionState1 == PackageManager.PERMISSION_GRANTED);
        } else {
            return false;
        }
    }

    /**
     * Permission 요청
     *
     * @param requestCode   RequestCode of Permissions
     */
    private void requestPermissions(int requestCode) {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    /**
     * Google play service 사용 가능 여부 검사
     *
     * @return  PlayService 사용 가능 여부
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mainActivity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                Timber.tag(TAG).e("[checkPlayServices] resultCode: %s", resultCode);
            } else {
                Timber.tag(TAG).e("[checkPlayServices] This device is not supported.");
            }
            return false;
        }
        return true;
    }

    /**
     * Get WorkType List
     *
     * Common Code 조회를 해서 가지고 온다.
     */
    public void getWorkTypeList() {
        showLoadingProgressBar();

        GetCommCodeListRequestDTO requestDTO = new GetCommCodeListRequestDTO();
        requestDTO.setGroupCode("WORK_TYPE_CD");

        Timber.tag(TAG).d("[getWorkTypeList] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<GetCommCodeListApiInterface> restClient = new DefaultRestClient<>();
        GetCommCodeListApiInterface getCommCodeListApiInterface = restClient.getClient(GetCommCodeListApiInterface.class);

        Call<GetCommCodeListResponseDTO> call = getCommCodeListApiInterface.getCommCodeList(requestDTO);
        call.enqueue(new Callback<GetCommCodeListResponseDTO>() {
            @Override
            public void onResponse(Call<GetCommCodeListResponseDTO> call, Response<GetCommCodeListResponseDTO> response) {
                if(response.isSuccessful()) {
                    GetCommCodeListResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[getWorkTypeList] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            if(responseDTO.getCommCodeList() != null) {
                                Timber.tag(TAG).d("[getWorkTypeList][CommCodeList] %s", Utils.convertObjToJSON(responseDTO.getCommCodeList()));

                                workTypeList = responseDTO.getCommCodeList();
                                PreferenceManager.setObject(mainActivity, "WorkTypeList", workTypeList);
                                hideLoadingProgressBar();
                            } else {
                                hideLoadingProgressBar();

                                if(mainActivity != null && !mainActivity.isFinishing()) {
                                    DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            if (mainActivity != null) {
                                                mainActivity.onBackPressed();
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            switch (responseDTO.getErrorType()) {
                                default: {
                                    hideLoadingProgressBar();

                                    if(mainActivity != null && !mainActivity.isFinishing()) {
                                        DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {
                                                if (mainActivity != null) {
                                                    mainActivity.onBackPressed();
                                                }
                                            }
                                        });
                                    }

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[getWorkTypeList] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        if(mainActivity != null && !mainActivity.isFinishing()) {
                            DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    if (mainActivity != null) {
                                        mainActivity.onBackPressed();
                                    }
                                }
                            });
                        }
                    }
                } else {
                    Timber.tag(TAG).e("[getWorkTypeList] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    if(mainActivity != null && !mainActivity.isFinishing()) {
                        DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                if (mainActivity != null) {
                                    mainActivity.onBackPressed();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCommCodeListResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[getWorkTypeList][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                if(mainActivity != null && !mainActivity.isFinishing()) {
                    DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (mainActivity != null) {
                                mainActivity.onBackPressed();
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * Get Task List
     */
//    public void getTaskList(final int position, final View view) {
    public void getTaskList() {
        showLoadingProgressBar();

        GetTaskListRequestDTO requestDTO = new GetTaskListRequestDTO();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREAN);
        SimpleDateFormat showDateFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.KOREAN);

        try {
            Date startDate = Calendar.getInstance().getTime();
            requestDTO.setStartDt(dateFormat.format(startDate.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Date endDate = Calendar.getInstance().getTime();
            requestDTO.setEndDt(dateFormat.format(endDate.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Timber.tag(TAG).d("[getTaskList] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<GetTaskListApiInterface> restClient = new DefaultRestClient<>();
        GetTaskListApiInterface getTaskListApiInterface = restClient.getClient(GetTaskListApiInterface.class);

        Call<GetTaskListResponseDTO> call = getTaskListApiInterface.getTaskList(requestDTO);
        call.enqueue(new Callback<GetTaskListResponseDTO>() {
            @Override
            public void onResponse(Call<GetTaskListResponseDTO> call, Response<GetTaskListResponseDTO> response) {
                if(response.isSuccessful()) {
                    GetTaskListResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[getTaskList] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            hideLoadingProgressBar();

                            if(responseDTO.getTaskList() != null) {
                                Timber.tag(TAG).d("[getTaskList][TaskList] %s", Utils.convertObjToJSON(responseDTO.getTaskList()));

                                if(taskList != null) {
                                    taskList.clear();
                                } else {
                                    taskList = new ArrayList<>();
                                }

                                List<TaskListItemDTO> tempList = responseDTO.getTaskList();
                                List<CommCodeDTO> cList = new ArrayList<>();
                                for(TaskListItemDTO item : tempList) {
                                    CommCodeDTO dto = new CommCodeDTO();
                                    dto.setCodeId(item.getId());
                                    dto.setCodeName(item.getId());
                                    dto.setMarkName(item.getTaskNm());
                                    cList.add(dto);
                                }

                                taskList.addAll(cList);
                                PreferenceManager.setObject(mainActivity, "TaskList", taskList);
                            } else {
                                if(mainActivity != null && !mainActivity.isFinishing()) {
                                    DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            mainActivity.onBackPressed();
                                        }
                                    });
                                }
                            }

                            setReLogInCount(0);
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if(getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_GET_TASK_LIST, "");
                                    } else {
                                        hideLoadingProgressBar();

                                        if(mainActivity != null && !mainActivity.isFinishing()) {
                                            DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(responseDTO.getMessage()));
                                        }

                                        setReLogInCount(0);
                                    }
                                    break;
                                }
                                default: {
                                    hideLoadingProgressBar();

                                    if(mainActivity != null && !mainActivity.isFinishing()) {
                                        DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(responseDTO.getMessage()), new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {
                                                mainActivity.onBackPressed();
                                            }
                                        });
                                    }

                                    setReLogInCount(0);
                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[getTaskList] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        if(mainActivity != null && !mainActivity.isFinishing()) {
                            DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    if (mainActivity != null) {
                                        mainActivity.onBackPressed();
                                    }
                                }
                            });
                        }

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[getTaskList] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    if(mainActivity != null && !mainActivity.isFinishing()) {
                        DialogHelper.showNormalAlertDialog(mainActivity, Utils.nullCheck(response.message()));
                    }

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<GetTaskListResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[getTaskList][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                if(mainActivity != null && !mainActivity.isFinishing()) {
                    DialogHelper.showNormalAlertDialog(mainActivity, "", new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (mainActivity != null) {
                                mainActivity.onBackPressed();
                            }
                        }
                    });
                }

                setReLogInCount(0);
            }
        });
    }
}