package com.pulmuone.knowledge.worker.schedule.bean;

import com.pulmuone.knowledge.worker.common.bean.BaseResponseDTO;

/**
 * Schedule Save Response DTO
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseResponseDTO
 */
public class ScheduleSaveResponseDTO extends BaseResponseDTO {
}
