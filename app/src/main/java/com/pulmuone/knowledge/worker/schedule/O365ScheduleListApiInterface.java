package com.pulmuone.knowledge.worker.schedule;

import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListByDateRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListByDateResponseDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Interface of Get Task List api
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         Call
 * @see         O365ScheduleListByDateRequestDTO
 * @see         O365ScheduleListByDateResponseDTO
 */
public interface O365ScheduleListApiInterface {
    @POST("/activityManager/employee/getMoffice365ScheduleList.do")
//    @POST("/activityManager/employee/getTestMoffice365ScheduleList.do ")
    Call<O365ScheduleListByDateResponseDTO> getO365ScheduleListByDate(@Body O365ScheduleListByDateRequestDTO dto);
}
