package com.pulmuone.knowledge.worker.main.bean;

import com.google.gson.annotations.SerializedName;
import com.pulmuone.knowledge.worker.common.bean.BaseRequestDTO;

/**
 * Add Work In/Out Request DTO
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseRequestDTO
 */
public class AddWorkInOutRequestDTO extends BaseRequestDTO {
    @SerializedName("wrkInOutStatus")
    String wrkInOutStatus;
    @SerializedName("latitudeNum")
    double latitudeNum;
    @SerializedName("longitudeNum")
    double longitudeNum;
    @SerializedName("addressInfo")
    String addressInfo;

    public String getWrkInOutStatus() {
        return wrkInOutStatus;
    }

    public void setWrkInOutStatus(String wrkInOutStatus) {
        this.wrkInOutStatus = wrkInOutStatus;
    }

    public double getLatitudeNum() {
        return latitudeNum;
    }

    public void setLatitudeNum(double latitudeNum) {
        this.latitudeNum = latitudeNum;
    }

    public double getLongitudeNum() {
        return longitudeNum;
    }

    public void setLongitudeNum(double longitudeNum) {
        this.longitudeNum = longitudeNum;
    }

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }
}
