package com.pulmuone.knowledge.worker.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pulmuone.knowledge.worker.common.bean.CommCodeDTO;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SharedPreference Manager
 *
 * @author      namki.an
 * @version     1.0.0
 */
public class PreferenceManager {
    /**
     * User ID 저장
     *
     * @param context   Context
     * @param id        To be saved User ID
     */
    public static void setUserId(Context context, String id) {
        try {
            SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("USER_ID", Utils.AES_Encode(id, AMSettings.SKEY));
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * User ID 가져오기
     *
     * @param context   Context
     * @return          Saved User ID
     */
    public static String getUserId(Context context) {
        try {
            SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
            String id = sp.getString("USER_ID", "");
            if(id.length() > 0) {
                return Utils.AES_Decode(id, AMSettings.SKEY);
            }

            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * User PWD 저장
     *
     * @param context   Context
     * @param pwd       To be saved User Password
     */
    public static void setUserPwd(Context context, String pwd) {
        try {
            SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("USER_PWD", Utils.AES_Encode(pwd, AMSettings.SKEY));
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * User PWD 가져오기
     *
     * @param context   Context
     * @return          Saved User Password
     */
    public static String getUserPwd(Context context) {
        try {
            SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
            String pwd = sp.getString("USER_PWD", "");
            if(pwd.length() > 0) {
                return Utils.AES_Decode(pwd, AMSettings.SKEY);
            }

            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
        SharedPreferences.Editor editor = sp.edit();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            jsonArray.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, jsonArray.toString());
        } else {
            editor.putString(key, null);
        }
        editor.apply();
    }

    public ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
        String strJson = sp.getString(key, null);
        ArrayList<String> urls = new ArrayList<>();
        if(strJson != null) {
            try{
                JSONArray jsonArray = new JSONArray(strJson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    String url = jsonArray.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return urls;
    }

    public static void setObject(Context context, String key,  List<CommCodeDTO> arrayList) {
        SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
        SharedPreferences.Editor editor = sp.edit();

        Gson gson = new Gson();
        String jsonObject = gson.toJson(arrayList);
        editor.putString(key, jsonObject);
        editor.apply();
    }

    public static List getObject(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(AMSettings.PREFS_INFO, 0);
        String jsonObject = sp.getString(key, null);
        Type type = new TypeToken<List<CommCodeDTO>>(){}.getType();
        Gson gson = new Gson();

        return gson.fromJson(jsonObject, type);
    }
}
