package com.pulmuone.knowledge.worker.auth.bean;

import com.pulmuone.knowledge.worker.common.bean.BaseResponseDTO;

/**
 * Logout Response DTO
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseResponseDTO
 */
public class LogoutResponseDTO extends BaseResponseDTO {
}
