package com.pulmuone.knowledge.worker.common.utils;

/**
 * RecyclerView Child Item Click Listener
 *
 * @author      namki.an
 * @version     1.0.0
 */
public interface OnChildItemClickListener<T> {
    void onChildItemClick(T item);
    void onChildItemClick(T item, int position);
    void onChildItemLongClick(T item, int position);
}
