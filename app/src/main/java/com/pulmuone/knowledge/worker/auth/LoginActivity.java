package com.pulmuone.knowledge.worker.auth;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;

import com.pulmuone.knowledge.worker.R;
import com.pulmuone.knowledge.worker.common.BaseActivity;
import com.github.ajalt.timberkt.Timber;

/**
 * Login Activity
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseActivity
 */
public class LoginActivity extends BaseActivity {
    private final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Set Status Bar Color to White
         */
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(LoginActivity.this, android.R.color.white));
        }

        setContentView(R.layout.activity_base);

        Timber.tag(TAG).i("onCreate");

        /**
         * Replace to LoginFragment
         */
        Fragment fragment = LoginFragment.getInstance();
        replaceFragmentSafely(fragment, String.valueOf(0), R.id.layout_content_holder, false, false, 0, 0, 0, 0);
    }
}
