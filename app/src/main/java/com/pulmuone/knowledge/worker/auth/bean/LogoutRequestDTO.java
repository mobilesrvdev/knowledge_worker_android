package com.pulmuone.knowledge.worker.auth.bean;

import com.pulmuone.knowledge.worker.common.bean.BaseRequestDTO;

/**
 * Logout Request DTO
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseRequestDTO
 */
public class LogoutRequestDTO extends BaseRequestDTO {
}
