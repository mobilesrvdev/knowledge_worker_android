package com.pulmuone.knowledge.worker.schedule.bean;

import com.google.gson.annotations.SerializedName;
import com.pulmuone.knowledge.worker.common.bean.BaseResponseDTO;

import java.util.List;

/**
 * Get O365 Schedule List Response DTO
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         BaseResponseDTO
 */
public class O365ScheduleSaveResponseDTO extends BaseResponseDTO {

}
