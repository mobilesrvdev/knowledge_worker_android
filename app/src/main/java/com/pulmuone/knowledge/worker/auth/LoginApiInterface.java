package com.pulmuone.knowledge.worker.auth;

import com.pulmuone.knowledge.worker.auth.bean.LoginRequestDTO;
import com.pulmuone.knowledge.worker.auth.bean.LoginResponseDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Interface of Login api
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         Call
 * @see         LoginRequestDTO
 * @see         LoginResponseDTO
 */
public interface LoginApiInterface {
    @POST("activityManager/api/common/loginConfirm.do")
    Call<LoginResponseDTO> doLogin(@Body LoginRequestDTO dto);
}
