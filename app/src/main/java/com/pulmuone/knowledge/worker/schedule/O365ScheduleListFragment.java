package com.pulmuone.knowledge.worker.schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ajalt.timberkt.Timber;
import com.pulmuone.knowledge.worker.R;
import com.pulmuone.knowledge.worker.auth.LoginApiInterface;
import com.pulmuone.knowledge.worker.auth.bean.LoginRequestDTO;
import com.pulmuone.knowledge.worker.auth.bean.LoginResponseDTO;
import com.pulmuone.knowledge.worker.common.AMSettings;
import com.pulmuone.knowledge.worker.common.BaseFragment;
import com.pulmuone.knowledge.worker.common.DefaultRestClient;
import com.pulmuone.knowledge.worker.common.DialogHelper;
import com.pulmuone.knowledge.worker.common.DummyRestClient;
import com.pulmuone.knowledge.worker.common.PreferenceManager;
import com.pulmuone.knowledge.worker.common.Utils;
import com.pulmuone.knowledge.worker.common.bean.CommCodeDTO;
import com.pulmuone.knowledge.worker.common.ui.DefaultBottomSheetMenuItemAdapter;
import com.pulmuone.knowledge.worker.common.utils.OnChildItemClickListener;
import com.pulmuone.knowledge.worker.main.MainActivity;
import com.pulmuone.knowledge.worker.main.otto_interfaces.Event_UpdateUserInfo;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListByDateRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListByDateResponseDTO;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListItemDTO;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleSaveResponseDTO;
import com.pulmuone.knowledge.worker.schedule.otto_interfaces.Event_UpdateSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * O365 Schedule List Fragment
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         BaseFragment
 */
public class O365ScheduleListFragment extends BaseFragment {
    private final String TAG = O365ScheduleListFragment.class.getSimpleName();

    private final int REQUEST_CODE_GET_O365_SCHEDULE_LIST_BY_DATE = 1;
    private final int REQUEST_CODE_SAVE_O365_SCHEDULE = 2;

    private ImageButton backButton;
    private RelativeLayout headerLayout;
    private ImageButton leftArrow, rightArrow;
    private TextView headerDateTextView;
    private Button addButton;

    private View mRecyclerViewTopBorder;
    private RecyclerView mRecyclerView;
    private LinearLayout emptyView;

    private Date currentDate;
    private SimpleDateFormat sdf;

    private O365ScheduleListAdapter mAdapter;

    private String taskRId = "";
    private String dutyCd = "";

    private List workTypeList;
    private List taskList;
    private List<O365ScheduleListItemDTO> listItemDTOS;
    private List<O365ScheduleListItemDTO> copyListItemDTOS;

    private BottomSheetDialog selectDialog;

    private final int SELECT_MODE_TASK = 10;
    private final int SELECT_MODE_WORK_TYPE = 11;

    public static final MediaType JSON
            = MediaType.parse("application/json");

    /**
     * Handler for WeakReference
     */
    private static class O365ScheduleListHandler extends Handler {
        private final WeakReference<O365ScheduleListFragment> weakReference;

        public O365ScheduleListHandler(O365ScheduleListFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        public O365ScheduleListFragment getWeakReference() {
            return weakReference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            O365ScheduleListFragment theFrag = weakReference.get();
            if(theFrag != null) {
                theFrag.handleMessage(msg);
            }
        }
    }

    private O365ScheduleListFragment.O365ScheduleListHandler mHandler = new O365ScheduleListFragment.O365ScheduleListHandler(this);

    private boolean canClick = true;

    private void handleMessage(Message msg) {
        // NOTHING
    }

    /**
     * 연속 클릭 방지
     */
    private void setCanClickable() {
        canClick = false;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                canClick = true;
            }
        }, 500);
    }


    /**
     * Default Constructor
     *
     * @return  O365 ScheduleListFragment
     */
    public static O365ScheduleListFragment getInstance() {
        O365ScheduleListFragment fragment = new O365ScheduleListFragment();
        return fragment;
    }

    private O365ScheduleListActivity o365ScheduleListActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof Activity) {
            o365ScheduleListActivity = (O365ScheduleListActivity) context;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_o365_schedule_list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.tag(TAG).i("onCreateView");

        View contentView = super.onCreateView(inflater, container, savedInstanceState);

        backButton = contentView.findViewById(R.id.o365_schedule_list_back_button);
        headerLayout = contentView.findViewById(R.id.o365_schedule_list_header_layout);
        addButton = contentView.findViewById(R.id.o365_schedule_list_top_bar_add_button);
        leftArrow = contentView.findViewById(R.id.o365_schedule_list_header_left_arrow);
        rightArrow = contentView.findViewById(R.id.o365_schedule_list_header_right_arrow);
        headerDateTextView = contentView.findViewById(R.id.o365_schedule_list_header_date_textview);
        mRecyclerViewTopBorder = contentView.findViewById(R.id.o365_schedule_list_recyclerview_top_divider);
        mRecyclerView = contentView.findViewById(R.id.o365_schedule_list_recyclerview);
        emptyView = contentView.findViewById(R.id.o365_schedule_list_empty_layout);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                o365ScheduleListActivity.onBackPressed();
            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPrevDate();
            }
        });

        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goNextDate();
            }
        });

        headerDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);

                new DatePickerDialog(o365ScheduleListActivity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        currentDate = calendar.getTime();

                        setCurrentDateString();
                        getO365ScheduleListByDate();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClick) {
                    setCanClickable();

                    if(isSaveValid()) {
                        new AlertDialog.Builder(o365ScheduleListActivity)
                                .setMessage(R.string.o365_schedule_save_confirm_message)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        // 팝업 확인 버튼 클릭 이벤트
                                        // Office 365 일정 가져와서 선택된 항목만 서버로 전송
                                        // 전송하기전에 CheckBox가 Check 된 항목이 몇 개 인지, Check 된 항목 중 TASK, 업무유형 이 선택되었는지 validation check 필요
                                        int checkCount = 0;
                                        boolean bCheck = true;
//                                        int bTimeCheckCount = 0;
//                                        if(copyListItemDTOS != null) {
//                                            copyListItemDTOS.clear();
//                                        }else  {
//                                            copyListItemDTOS = new ArrayList<>();
//                                        }
                                        for(int k = 0; k < listItemDTOS.size(); k++) {
                                            if(listItemDTOS.get(k).isCheckItem()) {
                                                checkCount++;
//                                                copyListItemDTOS.add(listItemDTOS.get(k));
                                                if(null == listItemDTOS.get(k).getWorkTypeMarkName()) { bCheck = false; }
                                                if(null == listItemDTOS.get(k).getTaskMarkName()) { bCheck = false; }
                                            }
                                        }
//                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//                                        Date startIdxStartTime = null;
//                                        Date startIdxEndTime = null;
//                                        Date copyIdxStartTime = null;
//                                        Date copyIdxEndTime = null;
                                        if(checkCount > 0) {
                                            if(bCheck) {
//                                                if(copyListItemDTOS.size() > 1){
//                                                    for(int startIdx = 0; startIdx < copyListItemDTOS.size(); startIdx++){
//                                                        for(int copyIdx = 0; copyIdx < copyListItemDTOS.size(); copyIdx++) {
//                                                            if(startIdx < copyIdx) {
//                                                                try {
//                                                                    startIdxStartTime = sdf.parse(copyListItemDTOS.get(startIdx).getStartDt() + copyListItemDTOS.get(startIdx).getStartTime() + "00");
//                                                                    startIdxEndTime = sdf.parse(copyListItemDTOS.get(startIdx).getEndDt() + copyListItemDTOS.get(startIdx).getEndTime() + "00");
//                                                                    copyIdxStartTime = sdf.parse(copyListItemDTOS.get(copyIdx).getStartDt() + copyListItemDTOS.get(copyIdx).getStartTime() + "00");
//                                                                    copyIdxEndTime = sdf.parse(copyListItemDTOS.get(copyIdx).getEndDt() + copyListItemDTOS.get(copyIdx).getEndTime() + "00");
//                                                                } catch (ParseException e) {
//                                                                    e.printStackTrace();
//                                                                }
//
//                                                                assert copyIdxStartTime != null;
//                                                                assert startIdxStartTime != null;
//                                                                assert startIdxEndTime != null;
//                                                                assert copyIdxEndTime != null;
//                                                                long lStartIdxStartTime = startIdxStartTime.getTime();
//                                                                long lStartIdxEndTime = startIdxEndTime.getTime();
//                                                                long lCopyIdxStartTime = copyIdxStartTime.getTime();
//                                                                long lCopyIdxEndTime = copyIdxEndTime.getTime();
//                                                                if(((lCopyIdxStartTime > lStartIdxStartTime) && (lCopyIdxStartTime < lStartIdxEndTime))
//                                                                        || ((lCopyIdxEndTime > lStartIdxStartTime) && (lCopyIdxEndTime < lStartIdxEndTime))) {
//                                                                    bTimeCheckCount++;
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                                // 시간 중복 체크 이후 전송
//                                                if(bTimeCheckCount == 0) {
//                                                    saveO365Schedule();
//                                                }else {
//                                                    DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, getResources().getString(R.string.o365_schedule_list_time_validation_message));
//                                                }
                                                saveO365Schedule();
                                            } else {
                                                DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, getResources().getString(R.string.o365_schedule_list_task_and_work_type_validation_message));
                                            }
                                        } else {
                                            DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, getResources().getString(R.string.o365_schedule_list_checkbox_validation_message));
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        // NOTHING
                                    }
                                }).create().show();
                    }
                }
            }
        });

        return contentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Timber.tag(TAG).i("onViewCreated");

        if(o365ScheduleListActivity != null) {
            o365ScheduleListActivity.startPostponedEnterTransition();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Timber.tag(TAG).i("onActivityCreated");

        // 오늘 날짜를 currentDate 로 설정
        currentDate = new Date();
        sdf = new SimpleDateFormat("yyyy.MM.dd(E)", Locale.KOREAN);
        setCurrentDateString();

        // Initialize RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(o365ScheduleListActivity));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(o365ScheduleListActivity), DividerItemDecoration.VERTICAL));

        workTypeList = PreferenceManager.getObject(o365ScheduleListActivity, "WorkTypeList");
        taskList = PreferenceManager.getObject(o365ScheduleListActivity, "TaskList");

        refreshList();
    }

    /**
     * 어제 날짜로 이동
     */
    private void goPrevDate() {
        Date prevDate = new Date(currentDate.getTime() - 24 * 60 * 60 * 1000);
        currentDate.setTime(prevDate.getTime());
        setCurrentDateString();

        refreshList();
    }

    /**
     * 내일 날짜로 이동
     */
    private void goNextDate() {
        Date prevDate = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
        currentDate.setTime(prevDate.getTime());
        setCurrentDateString();

        refreshList();
    }

    /**
     * 현재 날짜를 Header 에 보여준다.
     */
    private void setCurrentDateString() {
        String currentDateString = sdf.format(currentDate);
        headerDateTextView.setText(currentDateString);
    }

    /**
     * Empty View 보여주기 설정
     *
     * @param isShow    Is Show EmptyView
     */
    private void showEmptyView(boolean isShow) {
        if(isShow) {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    /**
     * Refresh List
     */
    public void refreshList() {
        setReLogInCount(0);
        getO365ScheduleListByDate();
    }

    /**
     * Retry of Get Schedule List
     */
    public void retryGetScheduleListByDate() {
        setReLogInCount(getReLogInCount() + 1);
        getO365ScheduleListByDate();
    }

    private OnChildItemClickListener<O365ScheduleListItemDTO> listItemClickListener = new OnChildItemClickListener<O365ScheduleListItemDTO>() {
        @Override
        public void onChildItemClick(O365ScheduleListItemDTO item) {

        }

        @Override
        public void onChildItemClick(O365ScheduleListItemDTO item, int position) {
            if(canClick) {
                setCanClickable();

                if(o365ScheduleListActivity != null) {

                    // do something
                    if(listItemDTOS != null && listItemDTOS.size() > 0) {
                        // 선택한 로우가 체크 ON 이면 체크 OFF
                        if(item.isCheckItem()) {
                            listItemDTOS.get(position).setCheckItem(false);
                        } else {
                            listItemDTOS.get(position).setCheckItem(true);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        }

        @Override
        public void onChildItemLongClick(O365ScheduleListItemDTO item, int position) {

        }
    };

    public void checkItem(final int position, final boolean isChecked){
        if(listItemDTOS != null && listItemDTOS.size() > 0) {
            listItemDTOS.get(position).setCheckItem(isChecked);
        }
    }

    /**
     * Get O365 Schedule List
     */
    private void getO365ScheduleListByDate() {
        if(listItemDTOS != null) {
            listItemDTOS.clear();
        }else {
            listItemDTOS = new ArrayList<>();
        }

//        if(copyListItemDTOS != null) {
//            copyListItemDTOS.clear();
//        }else  {
//            copyListItemDTOS = new ArrayList<>();
//        }

        showLoadingProgressBar();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREAN);
        final O365ScheduleListByDateRequestDTO requestDTO = new O365ScheduleListByDateRequestDTO();
        requestDTO.setEmail(PreferenceManager.getUserId(o365ScheduleListActivity));
        requestDTO.setSearchDate(dateFormat.format(currentDate));

        Timber.tag(TAG).d("[getO365ScheduleListByDate] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<O365ScheduleListApiInterface> restClient = new DefaultRestClient<>();
        O365ScheduleListApiInterface getListInterface = restClient.getClient(O365ScheduleListApiInterface.class);

        Call<O365ScheduleListByDateResponseDTO> call = getListInterface.getO365ScheduleListByDate(requestDTO);
        call.enqueue(new Callback<O365ScheduleListByDateResponseDTO>() {
            @Override
            public void onResponse(Call<O365ScheduleListByDateResponseDTO> call, Response<O365ScheduleListByDateResponseDTO> response) {
                if(response.isSuccessful()){
                    O365ScheduleListByDateResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[getO365Schedule] RESPONSE : [Success]%s::[ErrorType]%s::[Message]%s", responseDTO.isSuccess(), responseDTO.getErrorType(), responseDTO.getMessage());

                        if(responseDTO.isSuccess() && (responseDTO.getErrorType() == 200)) {
                            hideLoadingProgressBar();

                            if(responseDTO.getO365ScheduleList() != null && responseDTO.getO365ScheduleList().size() > 0) {
                                Timber.tag(TAG).d("[getO365ScheduleListByDate][O365ScheduleList] %s", Utils.convertObjToJSON(responseDTO.getO365ScheduleList()));

                                showEmptyView(false);
                                listItemDTOS = responseDTO.getO365ScheduleList();
                                mAdapter = new O365ScheduleListAdapter(listItemDTOS, listItemClickListener, getContext(), o365ScheduleListActivity, O365ScheduleListFragment.this);
                                mRecyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                            }else {
                                showEmptyView(true);
                            }

                            setReLogInCount(0);
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if (getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_GET_O365_SCHEDULE_LIST_BY_DATE, "");
                                    } else {
                                        hideLoadingProgressBar();

                                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));

                                        showEmptyView(true);

                                        setReLogInCount(0);
                                    }

                                    break;
                                }
                                default: {
                                    hideLoadingProgressBar();

                                    DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));

                                    showEmptyView(true);

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[getO365ScheduleListByDate] RESPONSE IS NULL");

                        hideLoadingProgressBar();

                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");

                        showEmptyView(true);

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[getO365ScheduleListByDate] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(response.message()));

                    showEmptyView(true);

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<O365ScheduleListByDateResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[getO365ScheduleListByDate][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");

                showEmptyView(true);

                setReLogInCount(0);
            }
        });
    }

    /**
     * Save O365 Schedule
     */
    private void saveO365Schedule() {
        showLoadingProgressBar();

        // JSON 형식으로 변환
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for(int i = 0; i < listItemDTOS.size(); i++){
                if(listItemDTOS.get(i).isCheckItem()) {

                    String startDt = String.valueOf(listItemDTOS.get(i).getStartDt());
                    String startTime = String.valueOf(listItemDTOS.get(i).getStartTime());

                    String endDt = String.valueOf(listItemDTOS.get(i).getEndDt());
                    String endTime = String.valueOf(listItemDTOS.get(i).getEndTime());

                    JSONObject subObject = new JSONObject();
                    subObject.put("startDt", startDt);
                    subObject.put("startTime", startTime);
                    subObject.put("endDt", endDt);
                    subObject.put("endTime", endTime);
                    subObject.put("taskId", listItemDTOS.get(i).getTaskCodeName());
                    subObject.put("dutyCd", listItemDTOS.get(i).getWorkTypeCodeName());
                    subObject.put("wrkDesc", listItemDTOS.get(i).getTitle());
                    jsonArray.put(subObject);
                }
            }
            jsonObject.put("scheduleList", jsonArray);
            Timber.tag(TAG).d("[saveO365Schedule][jsonObject] : %s", jsonObject.toString());
        } catch (JSONException e){
            e.printStackTrace();
        }

        DefaultRestClient<O365ScheduleSaveApiInterface> restClient = new DefaultRestClient<>();
        O365ScheduleSaveApiInterface saveApiInterface = restClient.getClient(O365ScheduleSaveApiInterface.class);

        RequestBody requestBody = RequestBody.create(JSON, String.valueOf(jsonObject));

        Call<O365ScheduleSaveResponseDTO> call = saveApiInterface.saveO365Schedule(requestBody);
        call.enqueue(new Callback<O365ScheduleSaveResponseDTO>() {
            @Override
            public void onResponse(Call<O365ScheduleSaveResponseDTO> call, Response<O365ScheduleSaveResponseDTO> response) {
                if(response.isSuccessful()) {
                    O365ScheduleSaveResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[saveO365Schedule] RESPONSE : [Success]%s::[ErrorType]%s::[Message]%s", responseDTO.isSuccess(), responseDTO.getErrorType(), responseDTO.getMessage());

                        if(responseDTO.isSuccess() && (responseDTO.getErrorType() == 200)) {
                            hideLoadingProgressBar();

                            if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                                DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, getResources().getString(R.string.schedule_save_complete_message), new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        postEvent(new Event_UpdateSchedule());

                                        setReLogInCount(0);

                                        if(o365ScheduleListActivity != null) {
                                            gotoScheduleList(currentDate);
                                        }
                                    }
                                });
                            }
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if(getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_SAVE_O365_SCHEDULE, "");
                                    } else {
                                        hideLoadingProgressBar();

                                        if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                                            DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));
                                        }

                                        setReLogInCount(0);
                                    }

                                    break;
                                }

                                default: {
                                    hideLoadingProgressBar();

                                    if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));
                                    }

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[saveO365Schedule] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                            DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");
                        }

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[saveO365Schedule] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, Utils.nullCheck(response.message()));
                    }

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<O365ScheduleSaveResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[saveO365Schedule][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                    DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");
                }

                setReLogInCount(0);
            }
        });
    }

    /**
     * Validation Check of Save
     *
     * @return  Can save
     */
    private boolean isSaveValid() {
        if (o365ScheduleListActivity == null) {
            return false;
        }

        return true;
    }

    @Override
    public void doAuthentication(final int requestCode, String stringParam) {
        LoginRequestDTO requestDTO = new LoginRequestDTO();
        requestDTO.setUserId(PreferenceManager.getUserId(o365ScheduleListActivity));
        requestDTO.setUserPW(Utils.EncBySha256(PreferenceManager.getUserPwd(o365ScheduleListActivity)));

        Timber.tag(TAG).d("[doAuthentication] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<LoginApiInterface> loginRestClient = new DefaultRestClient<>();
        LoginApiInterface loginInterface = loginRestClient.getClient(LoginApiInterface.class);

        Call<LoginResponseDTO> call = loginInterface.doLogin(requestDTO);
        call.enqueue(new Callback<LoginResponseDTO>() {
            @Override
            public void onResponse(Call<LoginResponseDTO> call, Response<LoginResponseDTO> response) {
                if(response.isSuccessful()) {
                    LoginResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[doAuthentication] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            String userInfoString = Utils.convertObjToJSON(responseDTO.getUserInfo());
                            Timber.tag(TAG).d("[doAuthentication][USER_INFO] %s", userInfoString);

                            String workInOutListString = Utils.convertObjToJSON(responseDTO.getWorkInOutList());
                            Timber.tag(TAG).d("[doAuthentication][WORK_IN_OUT] %s", workInOutListString);

                            // add code
                            // 로그인 시 유저 정보 갱신
                            postEvent(new Event_UpdateUserInfo(userInfoString, workInOutListString));

                            switch(requestCode) {
                                case REQUEST_CODE_GET_O365_SCHEDULE_LIST_BY_DATE: {
                                    retryGetScheduleListByDate();

                                    break;

                                }
                            }
                        } else {
                            switch (responseDTO.getErrorType()) {
                                default: {
                                    Timber.tag(TAG).e("[doAuthentication] ERROR TYPE : %s", responseDTO.getErrorType());

                                    hideLoadingProgressBar();

                                    if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, responseDTO.getMessage());
                                    }

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[doAuthentication] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                            DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");
                        }

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[doAuthentication] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                        DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, response.message());
                    }

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<LoginResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[doAuthentication][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                if(o365ScheduleListActivity != null && !o365ScheduleListActivity.isFinishing()) {
                    DialogHelper.showNormalAlertDialog(o365ScheduleListActivity, "");
                }

                setReLogInCount(0);
            }
        });
    }

    public void gotoScheduleList(Date currentDate) {
        // 선택된 Office 365 일정 등록 완료 후 메인 페이지로 이동
        Intent intent = new Intent();
        intent.setClass(o365ScheduleListActivity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("PREV_ACTIVITY", "O365ScheduleListActivity");
        intent.putExtra("CURRENT_DATE", currentDate);
        startActivity(intent);
        o365ScheduleListActivity.finish();
    }

    /**
     * Show BottomSheetDialog
     *
     * @param what  SELECT_MODE
     */
    public void showBottomSheetDialog(final int what, final int position, final View v) {
        if(o365ScheduleListActivity == null) return;

        selectDialog = new BottomSheetDialog(o365ScheduleListActivity);

        View view = o365ScheduleListActivity.getLayoutInflater().inflate(R.layout.bottomsheet_default_menu, null);

        Button cancelButton = view.findViewById(R.id.default_bottom_sheet_menu_cancel_button);

        RecyclerView recyclerView = view.findViewById(R.id.default_bottom_sheet_menu_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(o365ScheduleListActivity));

        switch(what) {
            case SELECT_MODE_TASK: {
                recyclerView.setAdapter(new DefaultBottomSheetMenuItemAdapter(taskList, new DefaultBottomSheetMenuItemAdapter.ItemListener() {
                    @Override
                    public void onItemClick(CommCodeDTO item) {
                        if(listItemDTOS != null && listItemDTOS.size() > 0) {
                            listItemDTOS.get(position).setTaskCodeId(item.getCodeId());
                            listItemDTOS.get(position).setTaskCodeName(item.getCodeName());
                            listItemDTOS.get(position).setTaskMarkName(item.getMarkName());
                            mAdapter.notifyDataSetChanged();
                        }

                        if(selectDialog != null) {
                            selectDialog.dismiss();
                        }
                    }
                }));
                break;
            }
            case SELECT_MODE_WORK_TYPE: {
                recyclerView.setAdapter(new DefaultBottomSheetMenuItemAdapter(workTypeList, new DefaultBottomSheetMenuItemAdapter.ItemListener() {
                    @Override
                    public void onItemClick(CommCodeDTO item) {
                        if(listItemDTOS != null && listItemDTOS.size() > 0) {
                            listItemDTOS.get(position).setWorkTypeCodeId(item.getCodeId());
                            listItemDTOS.get(position).setWorkTypeCodeName(item.getCodeName());
                            listItemDTOS.get(position).setWorkTypeMarkName(item.getMarkName());
                            mAdapter.notifyDataSetChanged();
                        }

                        if(selectDialog != null) {
                            selectDialog.dismiss();
                        }
                    }
                }));

                break;
            }
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectDialog != null) {
                    selectDialog.dismiss();
                }
            }
        });

        selectDialog.setContentView(view);

        // BottomSheetDialog 의 Height 가 wrap_content 인데 정상적으로 다 보여주지 못한다. 따라서 대충 1500 정도 줘서 다 올린다.
        BottomSheetBehavior behavior = BottomSheetBehavior.from((View)view.getParent());
        behavior.setPeekHeight(1500);

        selectDialog.show();
        selectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                selectDialog = null;

                switch(what) {
                    case SELECT_MODE_TASK:

                    case SELECT_MODE_WORK_TYPE:
                        if(v != null) {
                            v.setSelected(false);
                        }
                        break;
                }
            }
        });
    }
}
