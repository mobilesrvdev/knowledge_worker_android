package com.pulmuone.knowledge.worker.schedule.bean;

import com.google.gson.annotations.SerializedName;
import com.pulmuone.knowledge.worker.common.bean.BaseRequestDTO;

/**
 * Save O365 Schedule Request DTO
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         BaseRequestDTO
 */
public class O365ScheduleSaveRequestDTO extends BaseRequestDTO {

    @SerializedName(("id"))
    String id;
    @SerializedName("startDt")
    String startDt;
    @SerializedName("startTime")
    String startTime;
    @SerializedName("startHh")
    String startHh;
    @SerializedName("startMi")
    String startMi;
    @SerializedName("startDt")
    String endDt;
    @SerializedName("startTime")
    String endTime;
    @SerializedName("startHh")
    String endHh;
    @SerializedName("startMi")
    String endMi;
    @SerializedName("taskId")
    String taskId;
    @SerializedName("dutyCd")
    String dutyCd;
    @SerializedName("wrkDesc")
    String wrkDesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartHh() {
        return startHh;
    }

    public void setStartHh(String startHh) {
        this.startHh = startHh;
    }

    public String getStartMi() {
        return startMi;
    }

    public void setStartMi(String startMi) {
        this.startMi = startMi;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndHh() {
        return endHh;
    }

    public void setEndHh(String endHh) {
        this.endHh = endHh;
    }

    public String getEndMi() {
        return endMi;
    }

    public void setEndMi(String endMi) {
        this.endMi = endMi;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getDutyCd() {
        return dutyCd;
    }

    public void setDutyCd(String dutyCd) {
        this.dutyCd = dutyCd;
    }

    public String getWrkDesc() {
        return wrkDesc;
    }

    public void setWrkDesc(String wrkDesc) {
        this.wrkDesc = wrkDesc;
    }
}
