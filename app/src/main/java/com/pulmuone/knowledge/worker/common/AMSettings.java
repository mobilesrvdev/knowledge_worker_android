package com.pulmuone.knowledge.worker.common;

import java.util.HashSet;

/**
 * App 내부 설정 및 변수 모음
 *
 * @author      namki.an
 * @version     1.0.0
 */
public class AMSettings {
    public static boolean isDebugMode = false;

    /**
     * 출퇴근 기능 사용여부
     *
     * false 시 fragment_main.xml 에 Layout 도 변경해주어야 한다.
     */
    public static boolean useOnOffWork = true;

//    public static final String dev_baseUrl = "http://129.150.161.144:9073/";
//    public static final String dev_baseUrl = "https://amdev.pulmuone.com/";
    public static final String real_baseUrl = "https://am.pulmuone.com/";
    public static final String baseUrl = real_baseUrl;
//    public static final String baseUrl = dev_baseUrl;
    public static final String APP_DOWN_URL = baseUrl + "publishApp/";

    public static final int RELOGIN_RETRY_COUNT = 3;

    public static final String PREFS_INFO = "AM_PREFS_INFO";
    public static final String SKEY = "P87Auh13vUHxF2H4Bs0spA==";

    /**
     * 세션 이름(헤더에 사용)
     */
    public static String SESSION_NAME = "Cookie";

    /**
     * Cookie
     */
    public static HashSet<String> COOKIESET = new HashSet<>();

    /**
     * Error Type
     */
    public class ErrorType {
        public static final int SESSION_ERROR = 200410;
    }
}