package com.pulmuone.knowledge.worker.schedule.bean;

import com.google.gson.annotations.SerializedName;

/**
 * O365 Schedule List Item DTO
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 */
public class O365ScheduleListItemDTO {
    @SerializedName("id")
    private String id;
    @SerializedName("checkItem")
    private boolean checkItem;
    @SerializedName("startDt")
    private String startDt;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endDt")
    private String endDt;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("title")
    private String title;
    @SerializedName("taskCodeId")
    private String taskCodeId;
    @SerializedName("taskCodeName")
    private String taskCodeName;
    @SerializedName("taskMarkName")
    private String taskMarkName;
    @SerializedName("workTypeCodeId")
    private String workTypeCodeId;
    @SerializedName("workTypeCodeName")
    private String workTypeCodeName;
    @SerializedName("workTypeMarkName")
    private String workTypeMarkName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCheckItem() {
        return checkItem;
    }

    public void setCheckItem(boolean checkItem) {
        this.checkItem = checkItem;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTaskCodeId() {
        return taskCodeId;
    }

    public void setTaskCodeId(String taskCodeId) {
        this.taskCodeId = taskCodeId;
    }

    public String getTaskCodeName() {
        return taskCodeName;
    }

    public void setTaskCodeName(String taskCodeName) {
        this.taskCodeName = taskCodeName;
    }

    public String getTaskMarkName() {
        return taskMarkName;
    }

    public void setTaskMarkName(String taskMarkName) {
        this.taskMarkName = taskMarkName;
    }

    public String getWorkTypeCodeId() {
        return workTypeCodeId;
    }

    public void setWorkTypeCodeId(String workTypeCodeId) {
        this.workTypeCodeId = workTypeCodeId;
    }

    public String getWorkTypeCodeName() {
        return workTypeCodeName;
    }

    public void setWorkTypeCodeName(String workTypeCodeName) {
        this.workTypeCodeName = workTypeCodeName;
    }

    public String getWorkTypeMarkName() {
        return workTypeMarkName;
    }

    public void setWorkTypeMarkName(String workTypeMarkName) {
        this.workTypeMarkName = workTypeMarkName;
    }
}
