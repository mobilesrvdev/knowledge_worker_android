package com.pulmuone.knowledge.worker.schedule;

import org.json.JSONObject;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleSaveRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleSaveResponseDTO;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Interface of Get Task List api
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         Call
 * @see         O365ScheduleSaveRequestDTO
 * @see         O365ScheduleSaveResponseDTO
 */
public interface O365ScheduleSaveApiInterface {
    @POST("/activityManager/workSchedule/addMEmpScheduleGrid.do")
    Call<O365ScheduleSaveResponseDTO> saveO365Schedule(@Body RequestBody requestBody);
}
