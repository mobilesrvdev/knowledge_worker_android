package com.pulmuone.knowledge.worker.schedule.bean;

import com.google.gson.annotations.SerializedName;
import com.pulmuone.knowledge.worker.common.bean.BaseRequestDTO;

/**
 * Get O365 Schedule List Requesr DTO
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         BaseRequestDTO
 */
public class O365ScheduleListByDateRequestDTO extends BaseRequestDTO {

    @SerializedName("email")
    String email;

    @SerializedName("searchDate")
    String searchDate;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }
}
