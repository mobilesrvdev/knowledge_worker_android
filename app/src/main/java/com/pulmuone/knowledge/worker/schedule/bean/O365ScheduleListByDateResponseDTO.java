package com.pulmuone.knowledge.worker.schedule.bean;

import com.google.gson.annotations.SerializedName;
import com.pulmuone.knowledge.worker.common.bean.BaseResponseDTO;

import java.util.List;

/**
 * Get O365 Schedule List Response DTO
 *
 * @author      jeongsik.kim
 * @version     1.0.0
 * @see         BaseResponseDTO
 */
public class O365ScheduleListByDateResponseDTO extends BaseResponseDTO {
    @SerializedName("o365ScheduleList")
    List<O365ScheduleListItemDTO> o365ScheduleList;

    public List<O365ScheduleListItemDTO> getO365ScheduleList() {
        return o365ScheduleList;
    }

    public void setO365ScheduleList(List<O365ScheduleListItemDTO> scheduleList) {
        this.o365ScheduleList = scheduleList;
    }
}
