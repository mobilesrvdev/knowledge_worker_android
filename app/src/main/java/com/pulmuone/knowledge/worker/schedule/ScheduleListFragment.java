package com.pulmuone.knowledge.worker.schedule;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pulmuone.knowledge.worker.R;
import com.pulmuone.knowledge.worker.auth.LoginApiInterface;
import com.pulmuone.knowledge.worker.auth.bean.LoginRequestDTO;
import com.pulmuone.knowledge.worker.auth.bean.LoginResponseDTO;
import com.pulmuone.knowledge.worker.common.AMSettings;
import com.pulmuone.knowledge.worker.common.BaseFragment;
import com.pulmuone.knowledge.worker.common.DefaultRestClient;
import com.pulmuone.knowledge.worker.common.DialogHelper;
import com.pulmuone.knowledge.worker.common.PreferenceManager;
import com.pulmuone.knowledge.worker.common.Utils;
import com.pulmuone.knowledge.worker.common.utils.OnChildItemClickListener;
import com.pulmuone.knowledge.worker.main.otto_interfaces.Event_UpdateUserInfo;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleDeleteRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleDeleteResponseDTO;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleListByDateRequestDTO;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleListByDateResponseDTO;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleListItemDTO;
import com.github.ajalt.timberkt.Timber;
import com.pulmuone.knowledge.worker.schedule.otto_interfaces.Event_UpdateSchedule;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Schedule List Fragment
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         BaseFragment
 */
public class ScheduleListFragment extends BaseFragment {
    private final String TAG = ScheduleListFragment.class.getSimpleName();

    private final int REQUEST_CODE_GET_SCHEDULE_LIST_BY_DATE = 100;
    private final int REQUEST_CODE_DELETE_SCHEDULE = 102;

    private ImageButton backButton;
    private RelativeLayout headerLayout;
    private ImageButton leftArrow, rightArrow;
    private TextView headerDateTextView;
    private Button addButton;

    private View mRecyclerViewTopBorder;
    private RecyclerView mRecyclerView;
    private LinearLayout emptyView;

    private Date currentDate;
    private SimpleDateFormat sdf;

    private ScheduleListAdapter mAdapter;

    /**
     * Handler for WeakReference
     */
    private static class ScheduleListHandler extends Handler {
        private final WeakReference<ScheduleListFragment> weakReference;

        public ScheduleListHandler(ScheduleListFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        public ScheduleListFragment getWeakReference() {
            return weakReference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            ScheduleListFragment theFrag = weakReference.get();
            if(theFrag != null) {
                theFrag.handleMessage(msg);
            }
        }
    }

    private ScheduleListHandler mHandler = new ScheduleListHandler(this);

    private boolean canClick = true;

    private void handleMessage(Message msg) {
        // NOTHING
    }

    /**
     * 연속 클릭 방지
     */
    private void setCanClickable() {
        canClick = false;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                canClick = true;
            }
        }, 500);
    }


    /**
     * Default Constructor
     *
     * @return  ScheduleListFragment
     */
    public static ScheduleListFragment getInstance(Serializable currentDate) {
        ScheduleListFragment fragment = new ScheduleListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("CURRENT_DATE", currentDate);
        fragment.setArguments(bundle);
        return fragment;
    }

    private ScheduleListActivity scheduleListActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity) {
            scheduleListActivity = (ScheduleListActivity) context;
        }
    }

    /**
     * Get Root Layout
     *
     * @return Layout of this Fragment
     */
    @Override
    public int getLayout() {
        return R.layout.fragment_schedule_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.tag(TAG).i("onCreateView");

        View contentView = super.onCreateView(inflater, container, savedInstanceState);
        backButton = (ImageButton) contentView.findViewById(R.id.schedule_list_back_button);
        headerLayout = (RelativeLayout) contentView.findViewById(R.id.schedule_list_header_layout);
        addButton = (Button) contentView.findViewById(R.id.schedule_list_top_bar_add_button);
        leftArrow = (ImageButton) contentView.findViewById(R.id.schedule_list_header_left_arrow);
        rightArrow = (ImageButton) contentView.findViewById(R.id.schedule_list_header_right_arrow);
        headerDateTextView = (TextView) contentView.findViewById(R.id.schedule_list_header_date_textview);
        mRecyclerViewTopBorder = contentView.findViewById(R.id.schedule_list_recyclerview_top_divider);
        mRecyclerView = (RecyclerView) contentView.findViewById(R.id.schedule_list_recyclerview);
        emptyView = (LinearLayout) contentView.findViewById(R.id.schedule_list_empty_layout);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleListActivity.onBackPressed();
            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPrevDate();
            }
        });

        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goNextDate();
            }
        });

        headerDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);

                new DatePickerDialog(scheduleListActivity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        currentDate = calendar.getTime();

                        setCurrentDateString();
                        getScheduleListByDate();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(canClick) {
                    setCanClickable();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setClass(scheduleListActivity, ScheduleDetailActivity.class);
                    intent.putExtra("SCHEDULE_DATE", headerDateTextView.getText());
                    intent.putExtra("MODE", ScheduleDetailFragment.MODE_ADD);
                    startActivity(intent);
                }
            }
        });

        return contentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Timber.tag(TAG).i("onViewCreated");

        if(scheduleListActivity != null) {
            scheduleListActivity.startPostponedEnterTransition();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Timber.tag(TAG).i("onActivityCreated");

        currentDate = new Date();
        Bundle bundle = getArguments();
        Serializable current_date = null;
        if (bundle != null) {
            current_date = bundle.getSerializable("CURRENT_DATE");
        }
        if(current_date != null) {
            currentDate = (Date)current_date;
        }

        // 오늘 날짜를 currentDate 로 설정
//        currentDate = new Date();
        sdf = new SimpleDateFormat("yyyy.MM.dd(E)", Locale.KOREAN);
        setCurrentDateString();

        // Initialize RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(scheduleListActivity));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(scheduleListActivity), DividerItemDecoration.VERTICAL));

        refreshList();
    }

    /**
     * 어제 날짜로 이동
     */
    private void goPrevDate() {
        Date prevDate = new Date(currentDate.getTime() - 24 * 60 * 60 * 1000);
        currentDate.setTime(prevDate.getTime());
        setCurrentDateString();

        refreshList();
    }

    /**
     * 내일 날짜로 이동
     */
    private void goNextDate() {
        Date nextDate = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
        currentDate.setTime(nextDate.getTime());
        setCurrentDateString();

        refreshList();
    }

    /**
     * 현재 날짜를 Header 에 보여준다.
     */
    private void setCurrentDateString() {
        String currentDateString = sdf.format(currentDate);
        headerDateTextView.setText(currentDateString);
    }

    /**
     * Empty View 보여주기 설정
     *
     * @param isShow    Is Show EmptyView
     */
    private void showEmptyView(boolean isShow) {
        if(isShow) {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    private OnChildItemClickListener<ScheduleListItemDTO> listItemClickListener = new OnChildItemClickListener<ScheduleListItemDTO>() {
        @Override
        public void onChildItemClick(ScheduleListItemDTO item) {
            if(canClick) {
                setCanClickable();

                if(scheduleListActivity != null) {
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setClass(scheduleListActivity, ScheduleDetailActivity.class);
                    intent.putExtra("SCHEDULE_ID", item.getId());
                    intent.putExtra("MODE", ScheduleDetailFragment.MODE_MODIFY);
                    startActivity(intent);
                }
            }
        }

        @Override
        public void onChildItemClick(ScheduleListItemDTO item, int position) {

        }

        @Override
        public void onChildItemLongClick(final ScheduleListItemDTO item, int position) {
            if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                new AlertDialog.Builder(scheduleListActivity)
                        .setMessage(R.string.schedule_delete_confirm_message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteSchedule(item.getId());
//                                Toast.makeText(getContext(), "deleteSchedule", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // NOTHING
                            }
                        }).create().show();
            }
        }
    };

    /**
     * Refresh List
     */
    public void refreshList() {
        setReLogInCount(0);
        getScheduleListByDate();
    }

    /**
     * Retry of Get Schedule List
     */
    public void retryGetScheduleListByDate() {
        setReLogInCount(getReLogInCount() + 1);
        getScheduleListByDate();
    }

    /**
     * Retry of Delete Schedule
     */
    public void retryDeleteSchedule(String params) {
        setReLogInCount(getReLogInCount() + 1);
        deleteSchedule(params);
    }

    /**
     * Delete Schedule
     */
    private void deleteSchedule(final String id) {
        showLoadingProgressBar();

        ScheduleDeleteRequestDTO requestDTO = new ScheduleDeleteRequestDTO();
        requestDTO.setId(id);

        Timber.tag(TAG).d("[deleteSchedule] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<ScheduleDeleteApiInterface> restClient = new DefaultRestClient<>();
        ScheduleDeleteApiInterface deleteInterface = restClient.getClient(ScheduleDeleteApiInterface.class);

        Call<ScheduleDeleteResponseDTO> call = deleteInterface.deleteSchedule(requestDTO);
        call.enqueue(new Callback<ScheduleDeleteResponseDTO>() {
            @Override
            public void onResponse(Call<ScheduleDeleteResponseDTO> call, Response<ScheduleDeleteResponseDTO> response) {
                if(response.isSuccessful()) {
                    ScheduleDeleteResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[deleteSchedule] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            hideLoadingProgressBar();

                            postEvent(new Event_UpdateSchedule());

                            getScheduleListByDate();
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if (getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_DELETE_SCHEDULE, id);
                                    } else {
                                        hideLoadingProgressBar();

                                        if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                                            DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));
                                        }

                                        setReLogInCount(0);
                                    }
                                    break;
                                }
                                default: {
                                    hideLoadingProgressBar();

                                    if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                                        DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));
                                    }

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[deleteSchedule] RESPONSE BODY IS NULL");

                        hideLoadingProgressBar();

                        if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                            DialogHelper.showNormalAlertDialog(scheduleListActivity, "");
                        }

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[deleteSchedule] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                        DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(response.message()));
                    }

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<ScheduleDeleteResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[deleteSchedule][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                if(scheduleListActivity != null && !scheduleListActivity.isFinishing()) {
                    DialogHelper.showNormalAlertDialog(scheduleListActivity, "");
                }

                setReLogInCount(0);
            }
        });
    }

    /**
     * Get Schedule List
     */
    private void getScheduleListByDate() {
        showLoadingProgressBar();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREAN);

        ScheduleListByDateRequestDTO requestDTO = new ScheduleListByDateRequestDTO();
        requestDTO.setSearchDate(dateFormat.format(currentDate));

        Timber.tag(TAG).d("[getScheduleListByDate] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<ScheduleListApiInterface> restClient = new DefaultRestClient<>();
        ScheduleListApiInterface getListInterface = restClient.getClient(ScheduleListApiInterface.class);

        Call<ScheduleListByDateResponseDTO> call = getListInterface.getScheduleListByDate(requestDTO);
        call.enqueue(new Callback<ScheduleListByDateResponseDTO>() {
            @Override
            public void onResponse(Call<ScheduleListByDateResponseDTO> call, Response<ScheduleListByDateResponseDTO> response) {
                if(response.isSuccessful()) {
                    ScheduleListByDateResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[getScheduleListByDate] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            hideLoadingProgressBar();

                            if(responseDTO.getScheduleList() != null && responseDTO.getScheduleList().size() > 0) {
                                Timber.tag(TAG).d("[getScheduleListByDate][ScheduleList] %s", Utils.convertObjToJSON(responseDTO.getScheduleList()));

                                showEmptyView(false);

                                mAdapter = new ScheduleListAdapter(responseDTO.getScheduleList(), listItemClickListener);
                                mRecyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                            } else {
                                showEmptyView(true);
                            }

                            setReLogInCount(0);
                        } else {
                            switch (responseDTO.getErrorType()) {
                                case AMSettings.ErrorType.SESSION_ERROR: {
                                    if (getReLogInCount() <= AMSettings.RELOGIN_RETRY_COUNT) {
                                        doAuthentication(REQUEST_CODE_GET_SCHEDULE_LIST_BY_DATE, "");
                                    } else {
                                        hideLoadingProgressBar();

                                        DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));

                                        showEmptyView(true);

                                        setReLogInCount(0);
                                    }
                                    break;
                                }
                                default: {
                                    hideLoadingProgressBar();

                                    DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));

                                    showEmptyView(true);

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[getScheduleListByDate] RESPONSE IS NULL");

                        hideLoadingProgressBar();

                        DialogHelper.showNormalAlertDialog(scheduleListActivity, "");

                        showEmptyView(true);

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[getScheduleListByDate] RESPONSE FAIL code : %s", response.code());

                    hideLoadingProgressBar();

                    DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(response.message()));

                    showEmptyView(true);

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<ScheduleListByDateResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[getScheduleListByDate][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                DialogHelper.showNormalAlertDialog(scheduleListActivity, "");

                showEmptyView(true);

                setReLogInCount(0);
            }
        });
    }

    /**
     * Login
     *
     * @param requestCode   RequestCode of Login
     * @param stringParam   Param of Login
     */
    @Override
    public void doAuthentication(final int requestCode, final String stringParam) {
        LoginRequestDTO requestDTO = new LoginRequestDTO();
        requestDTO.setUserId(PreferenceManager.getUserId(scheduleListActivity));
        requestDTO.setUserPW(Utils.EncBySha256(PreferenceManager.getUserPwd(scheduleListActivity)));

        Timber.tag(TAG).d("[doAuthentication] REQUEST : %s", Utils.convertObjToJSON(requestDTO));

        DefaultRestClient<LoginApiInterface> loginRestClient = new DefaultRestClient<>();
        LoginApiInterface loginInterface = loginRestClient.getClient(LoginApiInterface.class);

        Call<LoginResponseDTO> call = loginInterface.doLogin(requestDTO);
        call.enqueue(new Callback<LoginResponseDTO>() {
            @Override
            public void onResponse(Call<LoginResponseDTO> call, Response<LoginResponseDTO> response) {
                hideLoadingProgressBar();

                if(response.isSuccessful()) {
                    LoginResponseDTO responseDTO = response.body();

                    if(responseDTO != null) {
                        Timber.tag(TAG).d("[doAuthentication] RESPONSE : %s", Utils.convertObjToJSON(responseDTO));

                        if(responseDTO.isSuccess()) {
                            String userInfoString = Utils.convertObjToJSON(responseDTO.getUserInfo());
                            Timber.tag(TAG).d("[doAuthentication][USER_INFO] %s", userInfoString);

                            String workInOutListString = Utils.convertObjToJSON(responseDTO.getWorkInOutList());
                            Timber.tag(TAG).d("[doAuthentication][WORK_IN_OUT] %s", workInOutListString);

                            postEvent(new Event_UpdateUserInfo(userInfoString, workInOutListString));

                            switch(requestCode) {
                                case REQUEST_CODE_GET_SCHEDULE_LIST_BY_DATE: {
                                    retryGetScheduleListByDate();

                                    break;
                                }

                                case REQUEST_CODE_DELETE_SCHEDULE: {
                                    retryDeleteSchedule(stringParam);

                                    break;
                                }
                            }
                        } else {
                            switch (responseDTO.getErrorType()) {
                                default: {
                                    Timber.tag(TAG).e("[doAuthentication] ERROR TYPE : %s", responseDTO.getErrorType());

                                    DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(responseDTO.getMessage()));

                                    setReLogInCount(0);

                                    break;
                                }
                            }
                        }
                    } else {
                        Timber.tag(TAG).e("[doAuthentication] RESPONSE IS NULL");

                        DialogHelper.showNormalAlertDialog(scheduleListActivity, "");

                        setReLogInCount(0);
                    }
                } else {
                    Timber.tag(TAG).e("[doAuthentication] RESPONSE FAIL code : %s", response.code());

                    DialogHelper.showNormalAlertDialog(scheduleListActivity, Utils.nullCheck(response.message()));

                    setReLogInCount(0);
                }
            }

            @Override
            public void onFailure(Call<LoginResponseDTO> call, Throwable t) {
                Timber.tag(TAG).e("[doAuthentication][FAIL] message : %s", t.getMessage());

                hideLoadingProgressBar();

                DialogHelper.showNormalAlertDialog(scheduleListActivity, "");

                setReLogInCount(0);
            }
        });
    }
}
