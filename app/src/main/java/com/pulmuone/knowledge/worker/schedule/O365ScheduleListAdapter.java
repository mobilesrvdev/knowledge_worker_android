package com.pulmuone.knowledge.worker.schedule;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pulmuone.knowledge.worker.R;
import com.pulmuone.knowledge.worker.common.DialogHelper;
import com.pulmuone.knowledge.worker.common.Utils;
import com.pulmuone.knowledge.worker.common.utils.OnChildItemClickListener;
import com.pulmuone.knowledge.worker.schedule.bean.O365ScheduleListItemDTO;
import com.pulmuone.knowledge.worker.schedule.bean.ScheduleListItemDTO;

import java.util.List;

/**
 * Schedule List Adapter
 *
 * @author      namki.an
 * @version     1.0.0
 * @see         android.support.v7.widget.RecyclerView.Adapter
 */
public class O365ScheduleListAdapter extends RecyclerView.Adapter<O365ScheduleListAdapter.ViewHolder> {
    private final String TAG = O365ScheduleListAdapter.class.getSimpleName();

    private OnChildItemClickListener<O365ScheduleListItemDTO> childItemClickListener;
    private List<O365ScheduleListItemDTO> mList;
    private Context context;
    private FragmentActivity activity;
    private O365ScheduleListFragment fragment;

    private final int SELECT_MODE_TASK = 10;
    private final int SELECT_MODE_WORK_TYPE = 11;

    /**
     * Default Constructor
     *
     * @param list          List
     * @param listener      ChildItemClickListener
     */
    public O365ScheduleListAdapter(List<O365ScheduleListItemDTO> list, OnChildItemClickListener listener, Context context, FragmentActivity activity, O365ScheduleListFragment fragment) {
        this.childItemClickListener = listener;
        this.activity = activity;
        this.mList = list;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_o365_schedule, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final O365ScheduleListItemDTO o365ScheduleListItemDTO = mList.get(position);

        String startDt = o365ScheduleListItemDTO.getStartDt();
        String startTime = o365ScheduleListItemDTO.getStartTime();

        String endDt = o365ScheduleListItemDTO.getEndDt();
        String endTime = o365ScheduleListItemDTO.getEndTime();
        StringBuilder stringBuilder = new StringBuilder();
        if(startDt.equals(endDt)) {
            stringBuilder.append(startDt, 0, 4).append(".")
                .append(startDt, 4, 6).append(".")
                .append(startDt, 6, 8).append("\n")
                .append(startTime, 0, 2).append(":")
                .append(startTime, 2, 4).append(" ~ ")
                .append(endTime, 0, 2).append(":")
                .append(endTime, 2, 4);
        }else {
            stringBuilder.append(startDt, 0, 4).append(".")
                .append(startDt, 4, 6).append(".")
                .append(startDt, 6, 8).append(" ")
                .append(startTime, 0, 2).append(":")
                .append(startTime, 2, 4).append(" ~ ")
                .append(endDt, 0, 4).append(".")
                .append(endDt, 4, 6).append(".")
                .append(endDt, 6, 8).append(" ")
                .append(endTime, 0, 2).append(":")
                .append(endTime, 2, 4);
        }

        holder.selectCheckBox.setOnCheckedChangeListener(null);

        holder.timeTextView.setText(stringBuilder);
        holder.titleTextView.setText(Utils.nullCheck(o365ScheduleListItemDTO.getTitle()));

        if(o365ScheduleListItemDTO.getWorkTypeMarkName() != null) {
            if (!o365ScheduleListItemDTO.getWorkTypeMarkName().equals("")) {
                holder.selectType.setText(Utils.nullCheck(o365ScheduleListItemDTO.getWorkTypeMarkName()));
            }
        }

        if(o365ScheduleListItemDTO.getTaskMarkName() != null) {
            if (!o365ScheduleListItemDTO.getTaskMarkName().equals("")) {
                holder.selectTask.setText(Utils.nullCheck(o365ScheduleListItemDTO.getTaskMarkName()));
            }
        }

        if(o365ScheduleListItemDTO.isCheckItem()) {
            holder.selectCheckBox.setChecked(true);
        }else {
            holder.selectCheckBox.setChecked(false);
        }

        holder.taskLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.selectCheckBox.isChecked()) {
                    v.setSelected(true);
                    fragment.showBottomSheetDialog(SELECT_MODE_TASK, position, v);
                }else {
                    DialogHelper.showNormalAlertDialog(activity, context.getResources().getString(R.string.o365_schedule_list_checkbox_validation_message));
                }
            }
        });

        holder.workTypeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.selectCheckBox.isChecked()) {
                    v.setSelected(true);
                    fragment.showBottomSheetDialog(SELECT_MODE_WORK_TYPE, position, v);
                }else {
                    DialogHelper.showNormalAlertDialog(activity, context.getResources().getString(R.string.o365_schedule_list_checkbox_validation_message));
                }
            }
        });

        holder.selectCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                fragment.checkItem(position, isChecked);
            }
        });

        holder.contentsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do something
                if(childItemClickListener != null && mList != null) {
                    childItemClickListener.onChildItemClick(o365ScheduleListItemDTO, position);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout contentsLayout;
        CheckBox selectCheckBox;
        TextView timeTextView;
        TextView titleTextView;
        TextView selectTask;
        TextView selectType;
        RelativeLayout taskLayout;
        RelativeLayout workTypeLayout;


        public ViewHolder(View itemView) {
            super(itemView);

            contentsLayout = itemView.findViewById(R.id.o365_schedule_list_item_contents_layout);
            selectCheckBox = itemView.findViewById(R.id.o365_schedule_list_item_checkbox);
            timeTextView = itemView.findViewById(R.id.o365_schedule_list_item_time_textview);
            titleTextView = itemView.findViewById(R.id.o365_schedule_list_item_title_textview);
            selectTask = itemView.findViewById(R.id.o365_schedule_detail_task_text);
            selectType = itemView.findViewById(R.id.o365_schedule_detail_work_type_text);
            taskLayout = itemView.findViewById(R.id.o365_schedule_detail_task_layout);
            workTypeLayout = itemView.findViewById(R.id.o365_schedule_detail_work_type_layout);
        }
    }
}
